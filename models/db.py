# -*- coding: utf-8 -*-

# Make sure changes in modules are recognized
from gluon.custom_import import track_changes
track_changes(True)

db = DAL(*DAL_ARGS, **DAL_KEYWORDS)

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
auth.settings.actions_disabled.append('register')

crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure auth policy
#auth.settings.registration_requires_verification = False
#auth.settings.registration_requires_approval = False
#auth.settings.reset_password_requires_verification = True
    


    

